FROM openjdk:8-alpine
#ARG JAR_FILE=target/VaccinationTrackingSystem-0.0.1-SNAPSHOT.jar
COPY VaccinationTrackingSystem-0.0.1-SNAPSHOT.jar /
EXPOSE 8080
#ENV JAVA_ARGS=""
ENTRYPOINT ["java", "-jar", "VaccinationTrackingSystem-0.0.1-SNAPSHOT.jar"]
#CMD java $JAVA_ARGS -jar VaccinationTrackingSystem-0.0.1-SNAPSHOT.jar
#FROM openjdk:8-alpine
#ARG JAR_FILE
#COPY target/${JAR_FILE} app.jar
#RUN mkdir -p /site/wwwroot/temp/
#RUN apk --no-cache add curl
#ENTRYPOINT ["java","-jar","/app.jar"]
#FROM java:8
#EXPOSE 8080
#ADD /target/demo.jar demo.jar
#ENTRYPOINT ["java","-jar","demo.jar"]