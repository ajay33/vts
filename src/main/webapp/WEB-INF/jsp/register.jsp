<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Sign Up</title>

<!-- Font Icon -->
<link href="fonts/material-icon/css/material-design-iconic-font.min.css" rel="stylesheet">
<link href="vendor/nouislider/nouislider.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/style.css.map" rel="stylesheet">


<!--<link rel="stylesheet"
	href="<c:url value="/resources/fonts/material-icon/css/material-design-iconic-font.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/resources/vendor/nouislider/nouislider.min.css"/>"> -->

<!-- Main css 
<link rel="stylesheet" href="<c:url value="/resources/css/style.css"/>">
<link rel="stylesheet"
	href="<c:url value="/resources/css/style.css.map"/>">-->
<!-- JS -->
<script src="jquery/jquery.min.js"></script>
<script src="vendor/nouislider/nouislider.min.js"></script>

<script src="vendor/wnumb/wNumb.js"></script>
<script src="js/main.js"></script>
<script src="vendor/jquery-validation/dist/additional-methods.min.js"></script>
<script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>

<style type="text/css">
img {
  width: 400px,
  height: auto
}</style>
</head>
<body>

	<div class="main">

		<div class="container">
			<div class="signup-content">
				<div class="signup-img">
					<img src="images/form-img.jpg"  alt="">
					<div class="signup-img-content">
						<h2>Register now</h2>
						<p>start vaccinating !</p>
					</div>
				</div>
				<div class="signup-form">
					<form method="post" action="postregister" class="register-form"
						id="register-form">
						<div class="form-row">
							<div class="form-group">
								<div class="form-input">
									<label for="name" class="required">Name</label> <input
										type="text" name="name" id="first_name" />
								</div>
								<div class="form-input">
									<label for="email" class="required">Email</label> <input
										type="text" name="email" id="email" />
								</div>



								<div class="form-input">
									<label for="phonenumber" class="required">Phone number</label>
									<input type="text" name="phonenumber" id="phonenumber" />
								</div>
								<div class="form-input">
									<label for="age" class="required">Age</label> <input
										type="text" name="age" id="age" />
								</div>
								<div class="form-input">
									<label for="address" class="required">Address</label> <input
										type="text" name="address" id="address" />
								</div>
								<div class="form-input">
									<label for="district" class="required">District</label> <input
										type="text" name="district" id="district" />
								</div>
							</div>
							<div class="form-group">

								<div class="form-radio">
									<div class="label-flex">
										<label for="payment" class="required">Gender</label>

									</div>
									<div class="form-radio-group">
										<div class="form-radio-item">
											<input type="radio" name="gender" id="cash" value="Male"
												checked> <label for="cash">Male</label> <span
												class="check"></span>
										</div>
										<div class="form-radio-item">
											<input type="radio" name="gender" value="Female" id="cheque">
											<label for="cheque">Female</label> <span class="check"></span>
										</div>
										<div class="form-radio-item">
											<input type="radio" name="gender" value="Transgender"
												id="demand"> <label for="demand">Transgender</label>
											<span class="check"></span>
										</div>
									</div>
								</div>
								<div class="form-input">
									<label for="state" class="required">State</label> <input
										type="text" name="state" id="state" />
								</div>
								<div class="form-input">
									<label for="pincode" class="required">Pincode</label> <input
										type="text" name="pincode" id="pincode" />
								</div>
								<div class="form-input">
									<label for="password" class="required">Password</label> <input
										type="password" name="password" id="password" />
								</div>
							</div>
						</div>

						<div class="form-submit">
							<input type="submit" value="Register" class="submit" id="submit"
								name="submit" /> <input type="submit" value="Reset"
								class="submit" id="reset" name="reset" />
								<h4>${error}</h4>
						</div>
						
					</form>
				</div>
			</div>
		</div>

	</div>



</body>
</html>