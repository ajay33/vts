<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>Booking Form HTML Template</title>

	<!-- Google font -->
	<link href="http://fonts.googleapis.com/css?family=Playfair+Display:900" rel="stylesheet" type="text/css" />
	<link href="http://fonts.googleapis.com/css?family=Alice:400,700" rel="stylesheet" type="text/css" />

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="detail/css/bootstrap.min.css" />

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="detail/css/style.css" />
<link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="Stylesheet"
        type="text/css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
  <script>
function myFunction() {
  alert("Click Ok to send details to your number:"+${u.phonenumber}+". You will be redirected to home");
}
</script>
<script language="javascript">
        $(document).ready(function () {
            $("#txtdate").datepicker({
            	dateFormat: "dd/mm/yy",
                minDate: 1,
               
                 
            });
        });
    </script>

</head>

<body>
	<div id="booking" class="section">
		<div class="section-center">
			<div class="container">
				<div class="row">
					<div class="booking-form">
						<div class="booking-bg">
							<div class="form-header">
								<h2>Make your reservation</h2>
								<p>A Message will be sent to your mobile number after clicking confirm</p>
							</div>
						</div>
						<form action="bookhospitalpost" method="post" >
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<span class="form-label">Date</span>
										
									<input type="text" name="date" class="form-control" id="txtdate" placeholder="Select Date">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="form-label">Time Slot</span>
										<select name="time" id="time" class="form-control">
											<option  selected>Select Time </option>
											<option value="9:00AM" >9:00AM </option>
											<option value="12:00PM">12:00PM</option>
											<option value="4:00PM">4:00PM</option>
											<option value="8:00AM">8:00PM</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<span class="form-label">Vaccine </span>
										
										<input type="text" value="${v}" disabled class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="form-label">Normal Bed</span>
										<input type="text" value="${n}" disabled class="form-control">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<span class="form-label">ICU Bed</span>
										
										<input type="text" value="${i}" disabled class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="form-label">Category</span>
										<select name="room" id="room" class="form-control" required>
											<option value="${c}" selected>${c}</option>
											
										</select>
										<span class="select-arrow"></span>
									</div>
								</div>
							</div>
							
							<div class="form-btn">
								<input type="submit" onclick="myFunction()" class="submit-btn" value="Confirm">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>


</html>