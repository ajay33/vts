package com.aj.hackathon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aj.hackathon.model.User;
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	 public User findByEmail(String email);

}
