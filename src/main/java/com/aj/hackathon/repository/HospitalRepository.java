package com.aj.hackathon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aj.hackathon.model.HospitalDetails;

public interface HospitalRepository extends JpaRepository<HospitalDetails, Long> {
 public List<HospitalDetails> findByHstate(String hstate);
}
