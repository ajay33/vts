package com.aj.hackathon.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.aj.hackathon.model.HospitalDetails;
import com.aj.hackathon.model.VaccineAndBed;
import com.aj.hackathon.repository.AdminRepository;
import com.aj.hackathon.repository.HospitalRepository;
import com.aj.hackathon.twilio.TwilioController;

@Service
public class AdminService {
	@Autowired
	AdminRepository arepo;
	@Autowired
	HospitalRepository hrepo;
	@Autowired
	TwilioController t;

	public void addetails(HospitalDetails h) {
		arepo.save(h);
		
	}

	public void deletehospitaldetails(Long hid) {
    arepo.deleteById(hid);		
	}

	public void updatedetails(VaccineAndBed h) {
		
		
	}

	public void updatedetails(Long hid, Long n, Long i, Long v) {
		List<HospitalDetails> h=hrepo.findAll().stream().filter(e ->e.getHid()==hid).collect(Collectors.toList());
		HospitalDetails h1=new HospitalDetails();
		h1.setHid(hid);
		h1.setHname(h.get(0).getHname());
		h1.setHcity(h.get(0).getHcity());
		h1.setHstate(h.get(0).getHstate());
		h1.setHvaccine_count(v);
		h1.setIhbeds_count(i);
		h1.setNhbeds_count(n);
		hrepo.save(h1);
		t.notify(hid,n,i,v);
		
	}

}
