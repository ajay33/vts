package com.aj.hackathon.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aj.hackathon.model.Checkout;
import com.aj.hackathon.model.HospitalDetails;
import com.aj.hackathon.model.User;
import com.aj.hackathon.repository.CheckoutRepository;
import com.aj.hackathon.repository.HospitalRepository;
import com.aj.hackathon.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository urepo;

	@Autowired
	CheckoutRepository crepo;

	@Autowired
	HospitalRepository hrepo;

	public void adduserdetails(User user) { 
		urepo.save(user);
	}

	public void addcheckout(HospitalDetails hospitalmodel, User usermodel, Checkout check,String category) {
		HospitalDetails h=hrepo.getOne(hospitalmodel.getHid());
		check.setHospitaldetails(hospitalmodel);
		check.setUser(usermodel);
		check.setDate(check.getDate()); 
		check.setTime(check.getTime());
		check.setOrdercreation(new Date());
		if(category.equalsIgnoreCase("MildCase"))
		{
			check.setNbednos(0L);
			check.setIbednos(0L);
			check.setVaccinenos(1L);
			List<HospitalDetails> list=hrepo.findAll().stream().filter(n->n.getHid()==hospitalmodel.getHid()).collect(Collectors.toList());
			list.stream().map(n->{
				n.setHvaccine_count(h.getHvaccine_count()-1);
				return n;
			}).collect(Collectors.toList());
			
			hrepo.save(list.get(0));
					
		}
		else if(category.equalsIgnoreCase("ModerateCase"))
		{
			check.setNbednos(1L);
			check.setIbednos(0L);
			check.setVaccinenos(1L);	
			List<HospitalDetails> list=hrepo.findAll().stream().filter(n->n.getHid()==hospitalmodel.getHid()).collect(Collectors.toList());
			list.stream().map(n->{
				n.setHvaccine_count(h.getHvaccine_count()-1);
				n.setNhbeds_count(h.getNhbeds_count()-1);
				return n;
			}).collect(Collectors.toList());
			
			hrepo.save(list.get(0));
		}
		else
		{
			check.setNbednos(0L);
			check.setIbednos(1L);
			check.setVaccinenos(1L);
			List<HospitalDetails> list=hrepo.findAll().stream().filter(n->n.getHid()==hospitalmodel.getHid()).collect(Collectors.toList());
			list.stream().map(n->{
				n.setHvaccine_count(h.getHvaccine_count()-1);
				n.setNhbeds_count(h.getNhbeds_count()-1);
				n.setIhbeds_count(h.getIhbeds_count()-1);
				return n;
			}).collect(Collectors.toList());
			
			hrepo.save(list.get(0));
		}
		
		
		check.setCategory(category);
		
		
        crepo.save(check);		
	}

}
