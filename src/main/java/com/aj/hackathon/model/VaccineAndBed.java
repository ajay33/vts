package com.aj.hackathon.model;

public class VaccineAndBed {
	private Long nbed;
	private Long hbed;
	private Long vaccine;
	public VaccineAndBed()
	{
		
	}
	public Long getNbed() {
		return nbed;
	}
	public void setNbed(Long nbed) {
		this.nbed = nbed;
	}
	public Long getHbed() {
		return hbed;
	}
	public void setHbed(Long hbed) {
		this.hbed = hbed;
	}
	public Long getVaccine() {
		return vaccine;
	}
	public void setVaccine(Long vaccine) {
		this.vaccine = vaccine;
	}
	public VaccineAndBed(Long nbed, Long hbed, Long vaccine) {
		super();
		this.nbed = nbed;
		this.hbed = hbed;
		this.vaccine = vaccine;
	}
	

}
